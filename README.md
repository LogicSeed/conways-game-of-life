# The Game of Life

The Game of Life is a cellular automaton created by John Conway in 1970. This computer program was written by Marc King to simulate Conway's Game of Life for Discrete Structures I - Project 1 (CIS275) at the University of Michigan - Dearborn during the Winter 2015 semester.
